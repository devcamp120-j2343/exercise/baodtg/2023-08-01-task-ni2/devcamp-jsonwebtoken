const express = require('express');
const cors = require('cors');

require('dotenv').config();

const db = require('./app/models')

const { initial } = require("./data");

const app = express();

app.use(cors());

app.use(express.json())

db.mongoose
    .connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`)
    .then(() => {
        console.log("Connect mongoDB Successfully");
        initial();
    })
    .catch((err) => {
        console.error('Connection error', err);
        process.exit();
    })

app.get('/', (req, res) => {
    res.json({
        message: 'Welcome to Devcamp JWT'
    })
})

app.use('/api/auth/', require('./app/routes/auth.routes'));
app.use('/api/user/', require('./app/routes/user.routes'));


const PORT = process.env.ENV_PORT || 8000;



app.listen(PORT, () => {
    console.log('App listening on port', PORT)
})