const getAllUser = (req, res) => {
    res.status(200).json({
        message: "Get All Users"
    })
}

const createUser = (req, res) => {
    res.status(201).json({
        message: 'Create User'
    })
}

const updateUser = (req, res) => {
    res.status(200).json({
        message: 'Update User'
    })
}

const deleteUser = (req, res) => {
    res.status(204).json({
        message: 'Delete User'
    })
}


module.exports = {
    getAllUser, 
    createUser,
    updateUser,
    deleteUser
}