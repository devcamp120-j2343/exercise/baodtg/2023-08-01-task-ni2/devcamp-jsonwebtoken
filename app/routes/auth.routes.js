const express = require('express');

const route = express.Router();

const authMiddleware = require('../middlewares/auth.middleware');
const authController = require('../controllers/auth.controller');

//hàm signUp chỉ dùng cho user
route.post("/signup", authMiddleware.checkDuplicateUsername, authController.signUp);

route.post("/login", authController.logIn);

route.post("/refreshToken", authController.refreshToken);

module.exports = route;