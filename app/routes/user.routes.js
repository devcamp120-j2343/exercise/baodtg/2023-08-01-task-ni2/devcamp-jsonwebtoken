const express = require('express');

const route = express.Router();

const userMiddleware = require('../middlewares/user.middleware')
const userController = require('../controllers/user.controller')

route.get("/", userController.getAllUser);
route.post("/",userMiddleware.verifyToken, userController.createUser);
route.put('/:userid',[userMiddleware.verifyToken, userMiddleware.checkUser], userController.updateUser);
route.delete('/:userid', userController.deleteUser)

module.exports = route
