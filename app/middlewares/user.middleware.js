const db = require("../models");
const jwt = require('jsonwebtoken');

const User = db.user;
const Role = db.role;

const verifyToken = async (req, res, next) => {
    try {
        console.log("verify token...");
        let token = req.headers['x-access-token'];
        console.log(token);
        if (!token) {
            return res.status(401).send({
                message: 'Không tìm thấy x-access-token !'
            })
        }

        const secretKey = process.env.JWT_SECRET;

        const verified = await jwt.verify(token, secretKey);
        console.log(verified);
        if (!verified) {
            return res.status(401).send({
                message: 'x-access-token không hợp lệ !'
            })
        }
        const user = await User.findById(verified.id).populate("roles");
        console.log(user);
        req.user = user;
        next();

    } catch (error) {
        console.log(error)
        return res.status(401).send({
            message: "Access token expired"
        });
    }
}

const checkUser = (req, res, next) => {
    console.log("check user...");
    const userRoles = req.user.roles;
    if (userRoles) {
        for (let i = 0; i < userRoles.length; i++) {
            if (userRoles[i].name == 'admin') {
                console.log('Authorized');
                next();
                return;

            }
        }
    }
    console.log("Unauthorized");
    return res.status(401).send({
        message: "Bạn không có quyền truy cập!"
    });

}



module.exports = {
    verifyToken,
    checkUser

}