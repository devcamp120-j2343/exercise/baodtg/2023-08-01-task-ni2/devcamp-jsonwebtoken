const db = require('../models')

const User = db.user;
const ROLES = db.ROLES;

const checkDuplicateUsername = async (req, res, next) => {
    try {
        const existUser = await User.findOne({
            userName: req.body.userName
        });
        if (existUser) {
            res.status(400).send({
                message: "Username is already in use"
            })
            return;
        }
        next();
    } catch (error) {

    }

}
module.exports = { checkDuplicateUsername }