const db = require('./app/models')


const initial = async () => {
    try {
        const roleModel = db.role;

        const count = await roleModel.estimatedDocumentCount();

        if (count === 0) {
            await roleModel({
                name: db.ROLES[0]
            }).save();
            await roleModel({
                name: db.ROLES[1]
            }).save();
            await roleModel({
                name: db.ROLES[2]
            }).save();

        }

    } catch (error) {
        console.error("Init data error", error);
        process.exit();
    }
}

module.exports = { initial }